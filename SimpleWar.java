public class SimpleWar{
	public static void main(String[] arg){
		runGame();
	}
	public static void runGame(){
		Deck warDeck = new Deck();
		warDeck.shuffle();
		//Represents the score of each card 
		double cardOneScore = 0.0;
		double cardTwoScore = 0.0;
		//Represents the points earned by each player who got an higher value card
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		//Loops till the deck is empty and displays the status of the players
		while(warDeck.length() > 0){
			Card playerOneCard = warDeck.drawTopCard();
			Card playerTwoCard = warDeck.drawTopCard();
			cardOneScore = playerOneCard.calculatePoints();
			cardTwoScore = playerTwoCard.calculatePoints();
			System.out.println("------------------------------------------------------------------------");
			System.out.println("Player 1's Card: " + playerOneCard + "\n Score: " + cardOneScore);
			System.out.println("Player 2's Card: " + playerTwoCard + "\n Score: " + cardTwoScore);
			if(cardOneScore > cardTwoScore){
				playerOnePoints++;
			}
			else if(cardOneScore < cardTwoScore){
				playerTwoPoints++;
			}
			else{
				playerOnePoints++;
				playerTwoPoints++;
			}
			System.out.println("**********************************");
			System.out.println("Point's Earned: \n Player 1: " + playerOnePoints + "\n Player 2: " +playerTwoPoints);
			System.out.println("**********************************");
			
			printFinalResult(playerOnePoints, playerTwoPoints);
		}
	}
	public static void printFinalResult(int playerOnePoints, int playerTwoPoints){
		//Prints congrats message depending on whether who wins
		if(playerOnePoints > playerTwoPoints){
			System.out.println("Player 1 wins, good job!");
		}
		else if(playerOnePoints < playerTwoPoints){
			System.out.println("Player 2 wins, good job!");
		}
		else{
			System.out.println("Its a tie");
		}
	}
}
import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;

	//Note: The constructor is a method, therefore can be treated as such
	public Deck(){
		this.numberOfCards = 52;
		this.cards = new Card[52];
		this.rng = new Random();
		int counter = 0;
		
		for(Suit suit: Suit.values()){
			for(Rank rank: Rank.values()){
				this.cards[counter] = new Card(suit,rank);
				counter++;
			}
		}
	}
	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	public String toString(){
		String cardString = "";
		for(int i = 0; i < numberOfCards; i++){
			cardString += this.cards[i] + "\n";
		}
		return cardString;
	}
	public void shuffle(){
		Card tempMemory = this.cards[0];
		for(int i = 0; i < numberOfCards; i++){
			int randLimit = numberOfCards - i;
			int randNum = rng.nextInt(randLimit);
			tempMemory = this.cards[randNum];
			this.cards[randNum] = this.cards[i];
			this.cards[i] = tempMemory;
		}
	}
}
public class Card{
	private Suit suit;
	private Rank value;

	public Card(Suit suit, Rank value){
		this.suit = suit;
		this.value = value;
	}
	public Suit getSuit(){
		return this.suit;
	}
	public Rank getValue(){
		return this.value;
	}
	public String toString(){
		return this.value + " of " + this.suit;
	}
	//Calculates the value of the cards as points
	public double calculatePoints(){
		double pointsEarned = 0.0;
		pointsEarned = this.value.getRankScore() + this.suit.getSuitScore();
		return pointsEarned;
	}	
}
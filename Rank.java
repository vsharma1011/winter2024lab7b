public enum Rank{
	Ace(1.0),
	Two(2.0),
	Three(3.0),
	Four(4.0),
	Five(5.0),
	Six(6.0),
	Seven(7.0),
	Eight(8.0),
	Nine(9.0),
	Ten(10.0),
	Jack(11.0),
	Queen(12.0),
	King(13.0);
	private double rankScore;
	//Why is public not allowed in the constructor?
	private Rank(double rankScore){
		this.rankScore = rankScore;
	}
	public double getRankScore(){
		return this.rankScore;
	}
}
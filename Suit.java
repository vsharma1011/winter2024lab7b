public enum Suit{
	Clubs(0.1),
	Diamonds(0.2),
	Spades(0.3),
	Hearts(0.4);

	private double suitScore;

	Suit(double suitScore){
		this.suitScore = suitScore;
	}
	public double getSuitScore(){
		return this.suitScore;
	}
}